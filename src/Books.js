import React from 'react'
import PropTypes from 'prop-types'

const Books = ({books, searchKeys}) => {
    const bookTableStyle = {
        table: {fontSize: '25px'},
        column: {width: '180px'}
    }
    return (
        <table style={bookTableStyle.table}>
            <thead>
                <tr>
                    {searchKeys.map((td, i) => <td key={i} style={bookTableStyle.column}>{td.toUpperCase()}</td>)}
                </tr>
            </thead>
            <tbody>
                {books.map((book, i) => <tr key={i}><td>{book.id}</td><td>{book.title}</td><td>{book.author}</td><td>{book.page}</td></tr>)}
            </tbody>
        </table>
    )
}

Books.propTypes = {
    Books: PropTypes.array,
    searchKeys: PropTypes.array
}

export default Books