import React, {Component} from 'react'
import Books from './Books'

class ReactApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: false,
            searchValue: '',
            searchKey: '',
            searchKeys: [],
            searchResult: [],
            books: []
        }
    }
    
    componentDidMount() {
        const books = [
            {id: 1, title: 'Kirikou le petit', author: 'Alain Bedi', page: 361},
            {id: 2, title: 'Mais enfant', author: 'Viviane Alonso', page: 157},
            {id: 3, title: "L'enfant noir", author: 'Dieu Donne', page: 751},
            {id: 4, title: "Mise a prix", author: 'Connor Blanc', page: 862},
        ]
        Array.isArray(books) && books.length !== 0 ? this.setState({searchKey: 'id', searchKeys: Object.keys(books[0]), books: books}) : this.setState({searchKeys: [], books: []})
    }

    newSearch = e => {
        e.target.value ? this.setState({search: true, searchValue: e.target.value}) : this.setState({search: false, searchValue: e.target.value})
        switch (this.state.searchKey) {
            case 'id':
                this.setState({searchResult: this.state.books.map((book) => {
                    return(
                        parseInt(book.id) === parseInt(e.target.value) ? book : {}
                    )
                })})
                break;
            case 'title':
                this.setState({searchResult: this.state.books.map((book) => {
                    return(
                        book.title.toLowerCase().includes(e.target.value.toLowerCase()) ? book : {}
                    )
                })})
                break;
            case 'author':
                this.setState({searchResult: this.state.books.map((book) => {
                    return(
                        book.author.toLowerCase().includes(e.target.value.toLowerCase()) ? book : {}
                    )
                })})
                break;
            case 'page':
                this.setState({searchResult: this.state.books.map((book) => {
                    return(
                        parseInt(book.page) === parseInt(e.target.value) ? book : {}
                    )
                })})
                break;

            default:
                break;
        }
    }

    newKey = e => this.setState({searchKey: e.target.value, searchValue: '', search: false})

    submit = e => e.preventDefault()

    render() {
        const searchStyle = {
            form: {
                fontSize: '25px',
                marginTop: '30px',
                marginBottom: '100px',
            },
            select: {
                fontSize: '20px'
            }
            
        }
        return (
            <div>
                <form onSubmit={this.submit} style={searchStyle.form} >
                    <label>Search By:</label>
                    <select value={this.state.searchKey} onChange={this.newKey} style={searchStyle.select} >
                        {this.state.searchKeys.map((key, i) => <option value={key} key={i}>{key}</option>)}
                    </select>
                    <input type='text' onChange={this.newSearch} value={this.state.searchValue} style={searchStyle.select} placeholder="Search..." />
                </form>
                {this.state.search ? <h1>Searching for: {this.state.searchValue}</h1> : <h1>Book List:</h1>}
                {this.state.search ? <Books books={this.state.searchResult} searchKeys={this.state.searchKeys} /> : <Books books={this.state.books} searchKeys={this.state.searchKeys} />}
            </div>
        )
    }
}

export default ReactApp